package me.vinceh121.bsr;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.metafetish.buttplug.core.ButtplugJsonMessageParser;
import org.metafetish.buttplug.core.messages.DeviceAdded;
import org.metafetish.buttplug.core.messages.DeviceFeature;
import org.metafetish.buttplug.core.messages.DeviceList;
import org.metafetish.buttplug.core.messages.DeviceMessageInfo;
import org.metafetish.buttplug.core.messages.Ok;
import org.metafetish.buttplug.core.messages.RequestServerInfo;
import org.metafetish.buttplug.core.messages.ServerInfo;

import com.fazecast.jSerialComm.SerialPort;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.impl.VertxFactory;
import io.vertx.ext.web.Router;
import me.vinceh121.buttplug4j.vertx.server.ButtplugMessageContext;
import me.vinceh121.buttplug4j.vertx.server.ButtplugWebSocketMessageHandler;
import me.vinceh121.buttplug4j.vertx.server.impl.ButtplugWebSocketMessageHandlerImpl;

public class BsrServer {
	public static int MSG_MODE = 0b01010100, MSG_OFF = 0b11110100, MSG_ON = 0b00110100;
	private final Vertx vertx;
	private final HttpServer server;
	private final Router router;
	private final ButtplugWebSocketMessageHandler buttplug;
	private final String serverInfo;

	private final SerialPort port;
	private final OutputStream out;
	private final AtomicInteger msgOffset = new AtomicInteger();

	public static void main(String[] args) {
		try {
			final BsrServer srv = new BsrServer();
			srv.start();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public BsrServer() throws IOException {
		this.vertx = new VertxFactory().vertx();
		this.vertx.exceptionHandler(Throwable::printStackTrace);
		this.server = this.vertx.createHttpServer();
		this.server.exceptionHandler(Throwable::printStackTrace);
		this.router = Router.router(this.vertx);
		this.server.requestHandler(this.router);
		this.buttplug = new ButtplugWebSocketMessageHandlerImpl();
		this.buttplug.addFailureHandler(Throwable::printStackTrace);
		this.serverInfo
				= new ButtplugJsonMessageParser().formatJson(new ServerInfo("Buttplug Serial Remote", 2, 2000, 1));

		this.router.get().handler(buttplug);
		this.router.errorHandler(500, ctx -> ctx.failure().printStackTrace());

		this.port = SerialPort.getCommPort("/dev/ttyUSB0");
		this.port.openPort();
		this.port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
		this.out = this.port.getOutputStream();

		this.buttplug.addMessageHandler("RequestServerInfo", ctx -> {
			final RequestServerInfo msg = (RequestServerInfo) ctx.getMessage();
			System.out.println("Client connected: " + msg.getClientName());
			ctx.getWebSocket().writeTextMessage(this.serverInfo);
		});

		this.buttplug.addMessageHandler("RequestDeviceList", ctx -> {
			final Map<String, DeviceFeature> msgs = new HashMap<>();
			msgs.put("VibrateCmd", new DeviceFeature(1, new int[] { 1, 2, 3 }));
			msgs.put("StopDeviceCmd", new DeviceFeature(1, new int[0]));
			final DeviceList res = new DeviceList();
			res.setId(ctx.getMessage().getId());
			res.setDevices(Collections.singletonList(new DeviceMessageInfo(0, "Serial", msgs)));
			try {
				ctx.getWebSocket().writeTextMessage(new ButtplugJsonMessageParser().formatJson(res));
			} catch (final IOException e) {
				e.printStackTrace();
			}
		});
		this.buttplug.addMessageHandler("StartScanning", ctx -> {
			try {
				ctx.getWebSocket()
						.writeTextMessage(new ButtplugJsonMessageParser().formatJson(new Ok(ctx.getMessage().getId())));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			this.vertx.setTimer(2000, l -> {
				final Map<String, DeviceFeature> msgs = new HashMap<>();
				msgs.put("VibrateCmd", new DeviceFeature(1, new int[] { 1, 2, 3 }));
				msgs.put("SingleMotorVibrateCmd", new DeviceFeature(1, new int[] { 1, 2, 3 }));
				msgs.put("StopDeviceCmd", new DeviceFeature(1, new int[0]));
				final DeviceAdded dev = new DeviceAdded(1, "Serial", msgs);
				try {
					ctx.getWebSocket().writeTextMessage(new ButtplugJsonMessageParser().formatJson(dev), v -> {
						// try {
						// ctx.getWebSocket()
						// .writeTextMessage(
						// new ButtplugJsonMessageParser().formatJson(new ScanningFinished()));
						// } catch (IOException e) {
						// e.printStackTrace();
						// }
					});
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		});

		this.buttplug.addMessageHandler("VibrateCmd", ctx -> {
			System.out.println("VIBRATE");
			try {
				this.out.write(MSG_MODE + msgOffset.get());
			} catch (IOException e) {
				e.printStackTrace();
			}
			tickOffset();
		});

		this.buttplug.addMessageHandler("StopDeviceCmd", this::handleStop);
		this.buttplug.addMessageHandler("StopAllDevices", this::handleStop);
	}

	private void handleStop(final ButtplugMessageContext ctx) {
		System.out.println("OFF");
		try {
			this.out.write(MSG_OFF + this.msgOffset.get());
		} catch (IOException e) {
			e.printStackTrace();
		}
		tickOffset();
	}

	private void tickOffset() {
		if (this.msgOffset.incrementAndGet() > 3) {
			this.msgOffset.set(0);
		}
	}

	public void start() {
		this.server.listen(12345); // XXX DEV PORT
	}
}
